﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Test.BL;


namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IFileHandler _fileHandler; 
        public MainWindow()
        {
            InitializeComponent();
            _fileHandler = new FileHandler();
        }

        private void FileOverview_Click(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(openFileDialog.ShowDialog() ==true)
            {
                _fileHandler.ReadFile(openFileDialog.FileName);
                Status_lable.Content = _fileHandler.AnalysisCoordinate();
                PathLine_txtBox.Text = openFileDialog.FileName;
                DrowArea.Children.Clear();
            }
            if(_fileHandler.Status == "Файл прочитан без ошибок")
            {
                _fileHandler.Drow();
                foreach (var figure in _fileHandler.shapes)
                    DrowArea.Children.Add(figure);
            }  
        }

        
    }
}
