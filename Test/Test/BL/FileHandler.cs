﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Test.BL
{
    internal class FileHandler : IFileHandler
    {
        public List<Shape> shapes { get; set; } = new List<Shape>();
        public string Status { get; set; }
        private List<string> file_Lines;
        private List<int[]> coordinates = new List<int[]>();
        public string AnalysisCoordinate()
        {

            for (int i = 0; i < file_Lines.Count; i++)
            {
                string[] line = file_Lines[i].Split(' ');
                if (String.IsNullOrWhiteSpace(string.Join("", line)))
                    continue;
                if (!line.All(l => l.Contains('-'))
                    && line.All(l => int.TryParse(l, out int digit)))
                {
                    InitCoordinates(line);
                    Status = "Файл прочитан без ошибок";
                }

                else
                {
                    Status = $"Ошибка! Строка: {i + 1}";
                    break;
                }
            }
            return Status;
        }
        public void ReadFile(string path)
        {
            file_Lines = File.ReadAllLines(path).ToList();
        }

        public void Drow()
        {
            foreach (var coordinate in coordinates)
            {
                if(coordinate.Length == 2)
                {
                    Point point = new Point(coordinate[0], coordinate[1]);

                }
                if (coordinate.Length == 4)
                {
                    Line line = new Line();
                    line.Stroke = Brushes.Black;
                    line.X1 = coordinate[0];
                    line.Y1 = coordinate[1];
                    line.X2 = coordinate[2];
                    line.Y2 = coordinate[3];
                    shapes.Add(line);
                }
            }
        }

        private void InitCoordinates(string[] line)
        {
            int[] arr = new int[line.Length];
            for (int i = 0; i < line.Length; i++)
                arr[i] = Convert.ToInt32(line[i]);
            coordinates.Add(arr);
        }
    }


}
