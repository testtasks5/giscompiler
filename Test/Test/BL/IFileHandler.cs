﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Test.BL
{
    internal interface IFileHandler
    {
        List<Shape> shapes { get; set; }
        string Status { get; set; }
        string AnalysisCoordinate();
        void ReadFile(string path);
        void Drow();
    }
}
